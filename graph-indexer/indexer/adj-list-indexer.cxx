#include <graph-indexer/indexer/adj-list-indexer.hxx>
#include <graph-indexer/aux/radix-sort.hxx>
#include <graph-indexer/aux/util.hxx>
#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/disk-dynamic-adj-list.hxx>
#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/mmap.hxx>
#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/sink.hxx>
#include <libgraph-processing/detail/static/zlib_sink.hxx>
#include <libgraph-processing/detail/traits.hxx>

#include <filesystem>
#include <fstream>
#include <iostream>

using namespace stg;
using namespace stg::detail;

template <typename Edge, typename Source, typename SinkGenerator>
static void update_adj_list_files(Source& source, SinkGenerator& sink_generator, std::ostream& out_index, direction_t direction, const adj_list_header& header);


adj_list_indexer::adj_list_indexer(const std::string& in,
                                   const std::string& type,
                                   bool has_weight, bool is_dynamic,
                                   bool is_compressed,
                                   bool remove_self_loops,
                                   bool outgoing,
                                   bool incoming,
                                   uint32_t base)
    : _in(in)
    , _is_direct(type == "direct")
    , _has_weight(has_weight)
    , _is_dynamic(is_dynamic)
    , _is_compressed(is_compressed)
    , _outgoing(outgoing)
    , _incoming(incoming)
    , _tmp_filename_out(gen_tmp(8))
    , _tmp_filename_in(gen_tmp(8))
{
    std::cout << "Converting to binary..." << std::endl;

    std::ifstream ifs(in);

    if(!ifs)
    {
        std::cerr << "Cannot open " << in << std::endl;
        exit(1);
    }

    auto extractor = [](auto edge) -> uint64_t { return edge.u; };

    if(_outgoing)
    {
        {

        std::ofstream ofs(_tmp_filename_out, std::ios::binary);

        if(_has_weight)
            cat_edge_stream_txt2bin<weighted_edge>(
                ifs, ofs,
                _is_direct, remove_self_loops);
        else
            cat_edge_stream_txt2bin<edge>(
                ifs, ofs,
                _is_direct, remove_self_loops);
        }


        std::cout << "Constructing outroing adjacency list..." <<std::endl;

        std::string tmp_filename2 = _tmp_filename_out + "2";
        std::filesystem::copy_file(_tmp_filename_out, tmp_filename2);

        if(_has_weight)
        {
            auto input_mapping = mmap_file<weighted_edge>(
                tmp_filename2);
            auto output_mapping = mmap_file<weighted_edge>(
                _tmp_filename_out);

            radix_sort(input_mapping.begin, input_mapping.end,
                       output_mapping.begin,
                       base, extractor);

            unmmap_file<weighted_edge>(input_mapping);
            unmmap_file<weighted_edge>(output_mapping);
        }
        else
        {
            auto input_mapping = mmap_file<edge>(tmp_filename2);
            auto output_mapping = mmap_file<edge>(_tmp_filename_out);

            radix_sort(input_mapping.begin, input_mapping.end,
                       output_mapping.begin,
                       base, extractor);

            unmmap_file<edge>(input_mapping);
            unmmap_file<edge>(output_mapping);
        }

        std::filesystem::remove(tmp_filename2);

        auto [v, e] =
            [has_weight = _has_weight, filename = _tmp_filename_out]() {
                if(has_weight)
                {
                    std::ifstream ifs(filename, std::ios::binary);
                    return find_VE<weighted_edge>(ifs);
                }
                else
                {
                    std::ifstream ifs(filename, std::ios::binary);
                    return find_VE<edge>(ifs);
                }
            }();

        _header.n_vertices = v;
        _header.n_edges = e;
    }

    if(_incoming && !_is_dynamic)
    {
        {

        std::ofstream ofs(_tmp_filename_in, std::ios::binary);

        bool swap = true;
        ifs.clear();
        ifs.seekg(0, std::ios::beg);

        if(_has_weight)
            cat_edge_stream_txt2bin<weighted_edge>(
                ifs, ofs,
                _is_direct, remove_self_loops, swap);
        else
            cat_edge_stream_txt2bin<edge>(
                ifs, ofs,
                _is_direct, remove_self_loops, swap);
        }


        std::cout << "Constructing incoming adjacency list..." <<std::endl;

        std::string tmp_filename2 = _tmp_filename_in + "2";
        std::filesystem::copy_file(_tmp_filename_in, tmp_filename2);

        if(_has_weight)
        {
            auto input_mapping = mmap_file<weighted_edge>(
                tmp_filename2);
            auto output_mapping = mmap_file<weighted_edge>(
                _tmp_filename_in);

            radix_sort(input_mapping.begin, input_mapping.end,
                       output_mapping.begin,
                       base, extractor);

            unmmap_file<weighted_edge>(input_mapping);
            unmmap_file<weighted_edge>(output_mapping);
        }
        else
        {
            auto input_mapping = mmap_file<edge>(tmp_filename2);
            auto output_mapping = mmap_file<edge>(_tmp_filename_in);

            radix_sort(input_mapping.begin, input_mapping.end,
                       output_mapping.begin,
                       base, extractor);

            unmmap_file<edge>(input_mapping);
            unmmap_file<edge>(output_mapping);
        }

        std::filesystem::remove(tmp_filename2);

        if(!_outgoing)
        {
            auto [v, e] =
                [has_weight = _has_weight, filename = _tmp_filename_out]() {
                    if(has_weight)
                    {
                        std::ifstream ifs(filename, std::ios::binary);
                        return find_VE<weighted_edge>(ifs);
                    }
                    else
                    {
                        std::ifstream ifs(filename, std::ios::binary);
                        return find_VE<edge>(ifs);
                    }
                }();

            _header.n_vertices = v;
            _header.n_edges = e;
        }
    }


    _header.type = graph_props_to_graph_type("adj-list",
        _is_direct ? "direct" : "undirect",
        _has_weight,
        _is_dynamic,
        _is_compressed,
        _sorted);

    _header.direction = _outgoing && !_incoming ?  OUTGOING :
        !_outgoing && _incoming ? INCOMING :
        _outgoing && _incoming ? OUTGOING_INCOMING : UNKNOWN;
}

void adj_list_indexer::sort(uint32_t base)
{
    auto extractor = [](auto edge) -> uint64_t { return edge.v; };

    if(_outgoing)
    {
        std::string tmp_filename2 = _tmp_filename_out + "2";
        std::filesystem::copy_file(_tmp_filename_out, tmp_filename2);

        if(_has_weight)
        {
            auto input_mapping = mmap_file<weighted_edge>(
                tmp_filename2);
            auto output_mapping = mmap_file<weighted_edge>(
                _tmp_filename_out);

            auto ibegin = input_mapping.begin;
            auto obegin = output_mapping.begin;
            uint32_t last_u = ibegin->u;

            for(auto iend = ibegin + 1; iend != input_mapping.end; ++iend)
            {
                uint32_t current_u = iend->u;

                if(current_u != last_u)
                {
                    radix_sort(ibegin, iend, obegin, base, extractor);

                    last_u = current_u;
                    obegin += iend - ibegin;
                    ibegin = iend;
                }
            }

            if(ibegin != input_mapping.end)
                radix_sort(ibegin, input_mapping.end, obegin, base, extractor);

            unmmap_file<weighted_edge>(input_mapping);
            unmmap_file<weighted_edge>(output_mapping);
        }
        else
        {
            auto input_mapping = mmap_file<edge>(tmp_filename2);
            auto output_mapping = mmap_file<edge>(_tmp_filename_out);

            auto ibegin = input_mapping.begin;
            auto obegin = output_mapping.begin;
            uint32_t last_u = ibegin->u;

            for(auto iend = ibegin + 1; iend != input_mapping.end; ++iend)
            {
                uint32_t current_u = iend->u;

                if(current_u != last_u)
                {
                    radix_sort(ibegin, iend, obegin, base, extractor);

                    last_u = current_u;
                    obegin += iend - ibegin;
                    ibegin = iend;
                }
            }

            if(ibegin != input_mapping.end)
                radix_sort(ibegin, input_mapping.end, obegin, base, extractor);

            unmmap_file<edge>(input_mapping);
            unmmap_file<edge>(output_mapping);
        }

        std::filesystem::remove(tmp_filename2);
    }

    if(_incoming)
    {
        std::string tmp_filename2 = _tmp_filename_in + "2";
        std::filesystem::copy_file(_tmp_filename_in, tmp_filename2);

        if(_has_weight)
        {
            auto input_mapping = mmap_file<weighted_edge>(
                tmp_filename2);
            auto output_mapping = mmap_file<weighted_edge>(
                _tmp_filename_in);

            auto ibegin = input_mapping.begin;
            auto obegin = output_mapping.begin;
            uint32_t last_u = ibegin->u;

            for(auto iend = ibegin + 1; iend != input_mapping.end; ++iend)
            {
                uint32_t current_u = iend->u;

                if(current_u != last_u)
                {
                    radix_sort(ibegin, iend, obegin, base, extractor);

                    last_u = current_u;
                    obegin += iend - ibegin;
                    ibegin = iend;
                }
            }

            if(ibegin != input_mapping.end)
                radix_sort(ibegin, input_mapping.end, obegin, base, extractor);

            unmmap_file<weighted_edge>(input_mapping);
            unmmap_file<weighted_edge>(output_mapping);
        }
        else
        {
            auto input_mapping = mmap_file<edge>(tmp_filename2);
            auto output_mapping = mmap_file<edge>(_tmp_filename_in);

            auto ibegin = input_mapping.begin;
            auto obegin = output_mapping.begin;
            uint32_t last_u = ibegin->u;

            for(auto iend = ibegin + 1; iend != input_mapping.end; ++iend)
            {
                uint32_t current_u = iend->u;

                if(current_u != last_u)
                {
                    radix_sort(ibegin, iend, obegin, base, extractor);

                    last_u = current_u;
                    obegin += iend - ibegin;
                    ibegin = iend;
                }
            }

            if(ibegin != input_mapping.end)
                radix_sort(ibegin, input_mapping.end, obegin, base, extractor);

            unmmap_file<edge>(input_mapping);
            unmmap_file<edge>(output_mapping);
        }

        std::filesystem::remove(tmp_filename2);
    }

    _sorted = false;
}

void adj_list_indexer::remove_duplicate()
{
    std::cout << "Removing duplicates..." << std::endl;

    if(_outgoing)
    {
        {
            std::ifstream ifs(_tmp_filename_out, std::ios::binary);
            std::ofstream ofs(_tmp_filename_out + "2", std::ios::binary);

            if(_has_weight)
                _header.n_edges = cat_edge_stream_nodup<weighted_edge>(ifs, ofs);
            else
                _header.n_edges = cat_edge_stream_nodup<edge>(ifs, ofs);
        }

        std::filesystem::remove(_tmp_filename_out); // ensure rename will not fail
        std::filesystem::rename(_tmp_filename_out + "2", _tmp_filename_out);
    }

    if(_incoming)
    {
        {
            std::ifstream ifs(_tmp_filename_in, std::ios::binary);
            std::ofstream ofs(_tmp_filename_in + "2", std::ios::binary);

            if(_has_weight)
                _header.n_edges = cat_edge_stream_nodup<weighted_edge>(ifs, ofs);
            else
                _header.n_edges = cat_edge_stream_nodup<edge>(ifs, ofs);
        }

        std::filesystem::remove(_tmp_filename_in); // ensure rename will not fail
        std::filesystem::rename(_tmp_filename_in + "2", _tmp_filename_in);
    }
}

void adj_list_indexer::make_index(const std::string& out)
{
    std::cout << "Indexing adjacency list... " << std::endl;

    if(!_is_dynamic)
    {
        std::ofstream output(out, std::ios::binary);

        output.write(reinterpret_cast<const char*>(&ADJ_LIST_MAGIC), sizeof(ADJ_LIST_MAGIC));
        output.write(reinterpret_cast<const char*>(&_header), sizeof(_header));

        off_t nil_pos = -1;

        if(_outgoing)
            for(size_t i = 0; i < _header.n_vertices; ++i)
                output.write(
                    reinterpret_cast<const char*>(&nil_pos),sizeof(nil_pos));

        if(_incoming)
            for(size_t i = 0; i < _header.n_vertices; ++i)
                output.write(
                    reinterpret_cast<const char*>(&nil_pos),sizeof(nil_pos));

        if(_outgoing)
        {
            {

            std::ifstream input(_tmp_filename_out, std::ios::binary);

            if(_has_weight)
            {
                stream_source<weighted_edge> source(input);

                if(_is_compressed)
                {
                    auto gen = [&output] () {
                        return zlib_stream_sink<weighted_neighbor>(output,
                                Z_DEFAULT_STRATEGY, output.tellp());
                    };

                    update_adj_list_files<weighted_edge>(
                            source, gen, output, OUTGOING, _header);
                }
                else
                {
                    auto gen = [&output] () {
                        return stream_sink<weighted_neighbor>(output,
                                output.tellp());
                    };

                    update_adj_list_files<weighted_edge>(
                            source, gen, output, OUTGOING, _header);
                }
            }
            else
            {
                stream_source<edge> source(input);

                if(_is_compressed)
                {
                    auto gen = [&output] () {
                        return zlib_stream_sink<neighbor> (output,
                                Z_DEFAULT_STRATEGY, output.tellp());
                    };

                    update_adj_list_files<edge>(
                            source, gen, output, OUTGOING, _header);
                }
                else
                {
                    auto gen = [&output] () {
                        return stream_sink<neighbor>(output, output.tellp());
                    };

                    update_adj_list_files<edge>(
                            source, gen, output, OUTGOING, _header);
                }
            }

            }

            std::filesystem::remove(_tmp_filename_out);
        }

        if(_incoming)
        {
            {

            std::ifstream input(_tmp_filename_in, std::ios::binary);

            if(_has_weight)
            {
                stream_source<weighted_edge> source(input);

                if(_is_compressed)
                {
                    auto gen = [&output] () {
                        return zlib_stream_sink<weighted_neighbor>
                            (output, Z_DEFAULT_STRATEGY, output.tellp());
                    };

                    update_adj_list_files<weighted_edge>(
                            source, gen, output, INCOMING, _header);
                }
                else
                {
                    auto gen = [&output] () {
                        return stream_sink<weighted_neighbor>(output, output.tellp());
                    };

                    update_adj_list_files<weighted_edge>(
                            source, gen, output, INCOMING, _header);
                }
            }
            else
            {
                stream_source<edge> source(input);

                if(_is_compressed)
                {
                    auto gen = [&output] () {
                        return zlib_stream_sink<neighbor> (output,
                                Z_DEFAULT_STRATEGY, output.tellp());
                    };

                    update_adj_list_files<edge>(
                            source, gen, output, INCOMING, _header);
                }
                else
                {
                    auto gen = [&output] () {
                        return stream_sink<neighbor>(output, output.tellp());
                    };

                    update_adj_list_files<edge>(
                            source, gen, output, INCOMING, _header);
                }
            }

            }

            std::filesystem::remove(_tmp_filename_in);
        }
    }
    else
    {
        std::ifstream input(_tmp_filename_out, std::ios::binary);
        std::filesystem::remove(out);

        if(_has_weight)
        {
            stream_source<weighted_edge> source(input);

            weighted_edge e;
            if(_is_compressed) // TODO compressed version
            {
                disk_dynamic_adj_list<weighted_edge> graph(out, _header.type, _header.direction);
                while(source.read(e))
                    graph.insert_edge(e);
            }
            else
            {
                disk_dynamic_adj_list<weighted_edge> graph(out, _header.type, _header.direction);
                while(source.read(e))
                    graph.insert_edge(e);
            }
        }
        else
        {
            stream_source<edge> source(input);
            edge e;
            if(_is_compressed)
            {
                disk_dynamic_adj_list<edge> graph(out, _header.type, _header.direction);
                while(source.read(e))
                    graph.insert_edge(e);
            }
            else
            {
                disk_dynamic_adj_list<edge> graph(out, _header.type, _header.direction);
                while(source.read(e))
                    graph.insert_edge(e);
            }
        }
    }
}


template <typename Edge, typename Source, typename SinkGenerator>
void update_adj_list_files(Source& source, SinkGenerator& sink_generator, std::ostream& out_index, direction_t direction, const adj_list_header& header)
{
    using Neighbor = neighbor_from_t<Edge>;

    uint32_t last_u = 0;
    std::ostringstream buffer(std::ios::binary);
    Edge e;
    Neighbor n;

    if(source.read(e))
    {
        last_u = e.u;
        auto neigh = edge_to_out_neighbor(e);
        buffer.write(reinterpret_cast<const char*>(&neigh), sizeof(neigh));
    }

    while(source.read(e))
    {
        if(e.u != last_u)
        {
            // works but its not ideal
            std::istringstream ib(buffer.str());
            buffer = std::ostringstream();

            auto sink = sink_generator();
            while(ib.read(reinterpret_cast<char*>(&n), sizeof(n)))
                sink.write(n);
            sink.flush();


            auto ipos = sizeof(ADJ_LIST_MAGIC) + sizeof(header) + sizeof(off_t) * last_u;
            if(direction == INCOMING && header.direction == OUTGOING_INCOMING)
                ipos += sizeof(off_t) * header.n_vertices;

            off_t cpos = out_index.tellp();

            out_index.seekp(ipos, std::ios::beg);
            out_index.write(reinterpret_cast<const char*>(&cpos),sizeof(cpos));
            out_index.seekp(cpos);

            last_u = e.u;
        }

        auto neigh = edge_to_out_neighbor(e);
        buffer.write(reinterpret_cast<const char*>(&neigh), sizeof(neigh));
    }

    if(buffer.tellp() > 0)
    {
        std::istringstream ib(buffer.str());

        auto sink = sink_generator();
        while(ib.read(reinterpret_cast<char*>(&n), sizeof(n)))
            sink.write(n);
        sink.flush();

        auto ipos = sizeof(ADJ_LIST_MAGIC) + sizeof(header) + sizeof(off_t) * last_u;
        if(direction == INCOMING && header.direction == OUTGOING_INCOMING)
            ipos += sizeof(off_t) * header.n_vertices;

        off_t cpos = out_index.tellp();

        out_index.seekp(ipos, std::ios::beg);
        out_index.write(reinterpret_cast<const char*>(&cpos),sizeof(cpos));
        out_index.seekp(cpos);
    }
}
