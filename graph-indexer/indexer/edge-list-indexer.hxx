#pragma once

#include <string>
#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/headers.hxx>

class edge_list_indexer
{
    std::string _in;

    bool _is_direct;
    bool _has_weight;
    bool _is_compressed;

    std::string _tmp_filename;
    stg::edge_list_header _header;

    bool _sorted = false;

public:

    edge_list_indexer(const std::string& in, const std::string& type,
                      bool has_weight, bool is_compressed,
                      bool remove_self_loops);

    void sort(uint32_t base);
    void remove_duplicate();
    void make_index(const std::string& out);
};
