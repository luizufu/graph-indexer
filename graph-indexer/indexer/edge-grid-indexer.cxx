#include <graph-indexer/indexer/edge-grid-indexer.hxx>
#include <graph-indexer/aux/radix-sort.hxx>
#include <graph-indexer/aux/util.hxx>
#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/disk-dynamic-edge-grid.hxx>
#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/mmap.hxx>
#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/sink.hxx>
#include <libgraph-processing/detail/static/zlib_sink.hxx>
#include <libgraph-processing/detail/static/zlib_source.hxx>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>


using namespace stg;
using namespace stg::detail;

static uint32_t get_grid_id(uint32_t u, uint32_t v,
                          uint32_t max, uint32_t grid_size);

template <typename Edge, typename Source, typename SinkGenerator>
static void update_edge_grid_files(Source& source, SinkGenerator& sink_generator, std::ostream& out_index, uint32_t max, const edge_grid_header& header);

edge_grid_indexer::edge_grid_indexer(const std::string& in,
                                     const std::string& type,
                                     bool has_weight, bool is_dynamic,
                                     bool is_compressed,
                                     bool remove_self_loops,
                                     uint32_t grid_size, uint32_t base, uint32_t max)
    : _in(in)
    , _is_direct(type == "direct")
    , _has_weight(has_weight)
    , _is_dynamic(is_dynamic)
    , _max(max)
    , _is_compressed(is_compressed)
    , _tmp_filename(gen_tmp(8))
{
    std::cout << "Converting to binary..." << std::endl;

    std::ifstream ifs(in);

    if(!ifs)
    {
        std::cerr << "Cannot open " << in << std::endl;
        exit(1);
    }

    {

    std::ofstream ofs(_tmp_filename, std::ios::binary);

    if(_has_weight)
        cat_edge_stream_txt2bin<weighted_edge>(
            ifs, ofs,
            _is_direct, remove_self_loops);
    else
        cat_edge_stream_txt2bin<edge>(
            ifs, ofs,
            _is_direct, remove_self_loops);
    }

    _header.type = graph_props_to_graph_type("edge-grid",
        _is_direct ? "direct" : "undirect",
        _has_weight,
        _is_dynamic,
        _is_compressed,
        _sorted);

    _header.grid_size = grid_size;

    if(!_is_dynamic)
    {

        std::cout << "Constructing grids of size: "
                  << grid_size << "x" << grid_size << "..." <<std::endl;

        auto [v, e] =
            [has_weight = _has_weight, filename = _tmp_filename]() {
                if(has_weight)
                {
                    std::ifstream ifs(filename, std::ios::binary);
                    return find_VE<weighted_edge>(ifs);
                }
                else
                {
                    std::ifstream ifs(filename, std::ios::binary);
                    return find_VE<edge>(ifs);
                }
            }();

        _max = v - 1;


        _header.n_vertices = v;
        _header.n_edges = e;

        auto extractor = [max = _max, grid_size](auto edge) -> uint64_t {
            return get_grid_id(
                    static_cast<uint32_t>(edge.e >> 32ul),
                    static_cast<uint32_t>(edge.e % 32ul),
                    max, grid_size);
        };

        std::string tmp_filename2 = _tmp_filename + "2";
        std::filesystem::copy_file(_tmp_filename, tmp_filename2);

        if(_has_weight)
        {
            auto input_mapping = mmap_file<weighted_edge>(
                    tmp_filename2);
            auto output_mapping = mmap_file<weighted_edge>(
                    _tmp_filename);

            radix_sort(input_mapping.begin, input_mapping.end,
                       output_mapping.begin,
                       base, extractor);

            unmmap_file<weighted_edge>(input_mapping);
            unmmap_file<weighted_edge>(output_mapping);
        }
        else
        {

            auto input_mapping = mmap_file<edge>(tmp_filename2);
            auto output_mapping = mmap_file<edge>(_tmp_filename);

            radix_sort(input_mapping.begin, input_mapping.end,
                       output_mapping.begin,
                       base, extractor);

            unmmap_file<edge>(input_mapping);
            unmmap_file<edge>(output_mapping);
        }

        std::filesystem::remove(tmp_filename2);
    }
}

void edge_grid_indexer::sort(uint32_t base)
{
    std::cout << "Sorting..." << std::endl;

    std::string tmp_filename2 = _tmp_filename + "2";
    std::filesystem::copy_file(_tmp_filename, tmp_filename2);

    auto extractor = [](auto edg) { return edg.e; };

    if(_has_weight)
    {
        auto input_mapping = mmap_file<weighted_edge>(tmp_filename2);
        auto output_mapping = mmap_file<weighted_edge>(_tmp_filename);

        auto ibegin = input_mapping.begin;
        auto obegin = output_mapping.begin;
        uint32_t last_grid_id = get_grid_id(ibegin->u, ibegin->v,_max,_header.grid_size);

        for(auto iend = ibegin + 1; iend != input_mapping.end; ++iend)
        {
            uint32_t current_grid_id = get_grid_id(iend->u,iend->v,_max,_header.grid_size);

            if(current_grid_id != last_grid_id)
            {
                radix_sort(ibegin, iend, obegin, base, extractor);

                last_grid_id = current_grid_id;
                obegin += iend - ibegin;
                ibegin = iend;
            }
        }

        if(ibegin != input_mapping.end)
            radix_sort(ibegin, input_mapping.end, obegin, base, extractor);

        unmmap_file<weighted_edge>(input_mapping);
        unmmap_file<weighted_edge>(output_mapping);
    }
    else
    {
        auto input_mapping = mmap_file<edge>(tmp_filename2);
        auto output_mapping = mmap_file<edge>(_tmp_filename);

        auto ibegin = input_mapping.begin;
        auto obegin = output_mapping.begin;
        uint32_t last_grid_id = get_grid_id(ibegin->u, ibegin->v,_max,_header.grid_size);

        for(auto iend = ibegin + 1; iend != input_mapping.end; ++iend)
        {
            uint32_t current_grid_id = get_grid_id(iend->u,iend->v,_max,_header.grid_size);

            if(current_grid_id != last_grid_id)
            {
                radix_sort(ibegin, iend, obegin, base, extractor);

                last_grid_id = current_grid_id;
                obegin += iend - ibegin;
                ibegin = iend;
            }
        }

        if(ibegin != input_mapping.end)
            radix_sort(ibegin, input_mapping.end, obegin, base, extractor);

        unmmap_file<edge>(input_mapping);
        unmmap_file<edge>(output_mapping);
    }

    std::filesystem::remove(tmp_filename2);

    _sorted = true;
}


void edge_grid_indexer::remove_duplicate()
{
    std::cout << "Removing duplicates..." << std::endl;

    {
        std::ifstream ifs(_tmp_filename, std::ios::binary);
        std::ofstream ofs(_tmp_filename + "2", std::ios::binary);

        if(_has_weight)
            _header.n_edges = cat_edge_stream_nodup<weighted_edge>(ifs, ofs);
        else
            _header.n_edges = cat_edge_stream_nodup<edge>(ifs, ofs);
    }

    std::filesystem::remove(_tmp_filename); // ensure rename will not fail
    std::filesystem::rename(_tmp_filename + "2", _tmp_filename);
}

void edge_grid_indexer::make_index(const std::string& out)
{
    std::cout << "Indexing edge grid..." << std::endl;

    if(!_is_dynamic)
    {
        std::ofstream output(out, std::ios::binary);

        output.write(reinterpret_cast<const char*>(&EDGE_GRID_MAGIC), sizeof(EDGE_GRID_MAGIC));
        output.write(reinterpret_cast<const char*>(&_header), sizeof(_header));

        off_t null_pos = -1;
        for(size_t i = 0; i < _header.grid_size * _header.grid_size; ++i)
            output.write(reinterpret_cast<const char*>(&null_pos),sizeof(null_pos));

        std::ifstream input(_tmp_filename, std::ios::binary);

        if(_has_weight)
        {
            stream_source<weighted_edge> source(input);

            if(_is_compressed)
            {
                auto gen = [&output] () {
                    return zlib_stream_sink<weighted_edge>
                        (output, Z_DEFAULT_STRATEGY, output.tellp());
                };

                update_edge_grid_files<weighted_edge>(source, gen, output,
                                                      _max, _header);
            }
            else
            {
                auto gen = [&output] () {
                    return stream_sink<weighted_edge>(
                            output, output.tellp());
                };

                update_edge_grid_files<weighted_edge>(source, gen, output,
                                                      _max, _header);
            }
        }
        else
        {
            stream_source<edge> source(input);

            if(_is_compressed)
            {
                auto gen =  [&output] () {
                    return zlib_stream_sink<edge>(
                            output, Z_DEFAULT_STRATEGY, output.tellp());
                };

                update_edge_grid_files<edge>(source, gen, output,
                                             _max, _header);
            }
            else
            {
                auto gen = [&output] () {
                    return stream_sink<edge>(output, output.tellp());
                };

                update_edge_grid_files<edge>(source, gen, output,
                                             _max, _header);
            }
        }


    }
    else
    {
        std::ifstream input(_tmp_filename, std::ios::binary);
        std::filesystem::remove(out);

        if(_has_weight)
        {
            stream_source<weighted_edge> source(input);

            weighted_edge e;

            if(_is_compressed) // TODO compressed version
            {
                disk_dynamic_edge_grid<weighted_edge> graph(
                        out, _header.type, _header.grid_size, _max);
                while(source.read(e))
                    graph.insert_edge(e);
            }
            else
            {
                disk_dynamic_edge_grid<weighted_edge> graph(
                        out, _header.type, _header.grid_size, _max);
                while(source.read(e))
                    graph.insert_edge(e);
            }
        }
        else
        {
            stream_source<edge> source(input);
            edge e;
            if(_is_compressed)
            {
                disk_dynamic_edge_grid<edge> graph(
                        out, _header.type, _header.grid_size, _max);
                while(source.read(e))
                    graph.insert_edge(e);
            }
            else
            {
                disk_dynamic_edge_grid<edge> graph(
                        out, _header.type, _header.grid_size, _max);
                while(source.read(e))
                    graph.insert_edge(e);
            }
        }
    }

    std::filesystem::remove(_tmp_filename);
}


template <typename Edge, typename Source, typename SinkGenerator>
void update_edge_grid_files(Source& source, SinkGenerator& sink_generator, std::ostream& out_index, uint32_t max, const edge_grid_header& header)
{
    uint32_t last_grid_id = 0;
    std::ostringstream buffer(std::ios::binary);
    Edge e, e2;

    if(source.read(e))
    {
        last_grid_id = get_grid_id(e.u, e.v, max, header.grid_size);
        buffer.write(reinterpret_cast<const char*>(&e), sizeof(e));
    }

    while(source.read(e))
    {
        uint32_t current_grid_id = get_grid_id(e.u, e.v, max, header.grid_size);

        if(current_grid_id != last_grid_id)
        {
            // works but is not ideal
            std::istringstream ib(buffer.str());
            buffer = std::ostringstream();

            auto sink = sink_generator();
            while(ib.read(reinterpret_cast<char*>(&e2), sizeof(e2)))
                sink.write(e2);
            sink.flush();

            auto ipos = sizeof(EDGE_GRID_MAGIC) + sizeof(edge_grid_header) + sizeof(off_t)*last_grid_id;
            off_t cpos = out_index.tellp();

            out_index.seekp(ipos, std::ios::beg);
            out_index.write(reinterpret_cast<const char*>(&cpos),sizeof(cpos));
            out_index.seekp(cpos);

            last_grid_id = current_grid_id;
        }

        buffer.write(reinterpret_cast<const char*>(&e), sizeof(e));
    }

    if(buffer.tellp() > 0)
    {
        std::istringstream ib(buffer.str());

        auto sink = sink_generator();
        while(ib.read(reinterpret_cast<char*>(&e2), sizeof(e2)))
            sink.write(e2);
        sink.flush();

        auto ipos = sizeof(EDGE_GRID_MAGIC) + sizeof(edge_grid_header) + sizeof(off_t)*last_grid_id;
        off_t cpos = out_index.tellp();

        out_index.seekp(ipos, std::ios::beg);
        out_index.write(reinterpret_cast<const char*>(&cpos),sizeof(cpos));
        out_index.seekp(cpos);
    }
}

uint32_t get_grid_id(uint32_t u, uint32_t v, uint32_t max, uint32_t grid_size)
{
    uint32_t piece = (max + 1) / grid_size;
    uint32_t row = u / piece;
    uint32_t column = v / piece;

    return row * grid_size + column;
}

