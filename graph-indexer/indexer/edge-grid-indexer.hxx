#pragma once

#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/headers.hxx>

#include <string>


class edge_grid_indexer
{
    std::string _in;

    bool _is_direct;
    bool _has_weight;
    bool _is_dynamic;
    bool _is_compressed;

    std::string _tmp_filename;

    uint32_t _max;
    stg::edge_grid_header _header;
    bool _sorted = false;

public:

    edge_grid_indexer(const std::string& in, const std::string& type,
                      bool has_weight, bool is_dynamic, bool is_compressed,
                      bool remove_self_loops,
                      uint32_t grid_size, uint32_t base,
                      uint32_t max);

    void sort(uint32_t base);
    void remove_duplicate();
    void make_index(const std::string& out);
};
