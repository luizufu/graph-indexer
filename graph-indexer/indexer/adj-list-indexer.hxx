#pragma once

#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/headers.hxx>
#include <string>

class adj_list_indexer
{
    std::string _in;

    bool _is_direct;
    bool _has_weight;
    bool _is_dynamic;
    bool _is_compressed;
    bool _outgoing;
    bool _incoming;

    std::string _tmp_filename_out;
    std::string _tmp_filename_in;
    stg::adj_list_header _header;

    bool _sorted = false;

public:

    adj_list_indexer(const std::string& in, const std::string& type,
                     bool has_weight, bool is_dynamic, bool is_compressed,
                     bool remove_self_loops,
                     bool outgoing, bool incoming,
                     uint32_t base);

    void sort(uint32_t base);
    void remove_duplicate();

    void make_index(const std::string& out);
};
