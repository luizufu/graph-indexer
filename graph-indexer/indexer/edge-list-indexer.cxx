#include <graph-indexer/indexer/edge-list-indexer.hxx>
#include <graph-indexer/aux/radix-sort.hxx>
#include <graph-indexer/aux/util.hxx>
#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/mmap.hxx>
#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/zlib_sink.hxx>
#include <libgraph-processing/detail/static/zlib_source.hxx>

#include <filesystem>
#include <fstream>
#include <iostream>

using namespace stg;
using namespace stg::detail;

template <typename Edge, typename Source, typename Sink>
static void cat_source_to_sink(Source& source, Sink& sink);


edge_list_indexer::edge_list_indexer(const std::string& in,
                                     const std::string& type,
                                     bool has_weight, bool is_compressed,
                                     bool remove_self_loops)
    : _in(in)
    , _is_direct(type == "direct")
    , _has_weight(has_weight)
    , _is_compressed(is_compressed)
    , _tmp_filename(gen_tmp(8))
{
    std::cout << "Converting to binary..." << std::endl;

    std::ifstream ifs(in);

    if(!ifs)
    {
        std::cerr << "Cannot open " << in << std::endl;
        exit(1);
    }

    {
        std::ofstream ofs(_tmp_filename, std::ios::binary);

        if(_has_weight)
            cat_edge_stream_txt2bin<weighted_edge>(
                ifs, ofs,
                _is_direct, remove_self_loops);
        else
            cat_edge_stream_txt2bin<edge>(
                ifs, ofs,
                _is_direct, remove_self_loops);
    }

    auto [n_vertices, n_edges] =
        [has_weight = _has_weight, filename = _tmp_filename]() {
            if(has_weight)
            {
                std::ifstream ifs(filename, std::ios::binary);
                return find_VE<weighted_edge>(ifs);
            }
            else
            {
                std::ifstream ifs(filename, std::ios::binary);
                return find_VE<edge>(ifs);
            }
        }();

    _header.type = graph_props_to_graph_type("edge-list",
        _is_direct ? "direct" : "undirect",
        _has_weight,
        true,
        _is_compressed,
        _sorted);
    _header.n_vertices = n_vertices;
    _header.n_edges = n_edges;

}

void edge_list_indexer::sort(uint32_t base)
{
    std::cout << "Sorting..." << std::endl;

    std::string tmp_filename2 = _tmp_filename + "2";
    std::filesystem::copy_file(_tmp_filename, tmp_filename2);

    auto extractor = [](auto edge) { return edge.e; };

    if(_has_weight)
    {
        auto input_mapping = mmap_file<weighted_edge>(tmp_filename2);
        auto output_mapping = mmap_file<weighted_edge>(_tmp_filename);

        radix_sort(input_mapping.begin, input_mapping.end,
                   output_mapping.begin,
                   base, extractor);

        unmmap_file<weighted_edge>(input_mapping);
        unmmap_file<weighted_edge>(output_mapping);
    }
    else
    {
        auto input_mapping = mmap_file<edge>(tmp_filename2);
        auto output_mapping = mmap_file<edge>(_tmp_filename);

        radix_sort(input_mapping.begin, input_mapping.end,
                   output_mapping.begin,
                   base, extractor);

        unmmap_file<edge>(input_mapping);
        unmmap_file<edge>(output_mapping);
    }


    std::filesystem::remove(tmp_filename2);

    _sorted = true;
}

void edge_list_indexer::remove_duplicate()
{
    std::cout << "Removing duplicates..." << std::endl;

    {
        std::ifstream ifs(_tmp_filename, std::ios::binary);
        std::ofstream ofs(_tmp_filename + "2", std::ios::binary);

        if(_has_weight)
            _header.n_edges = cat_edge_stream_nodup<weighted_edge>(ifs, ofs);
        else
            _header.n_edges = cat_edge_stream_nodup<edge>(ifs, ofs);
    }

    std::filesystem::remove(_tmp_filename); // ensure rename will not fail
    std::filesystem::rename(_tmp_filename + "2", _tmp_filename);
}

void edge_list_indexer::make_index(const std::string& out)
{
    std::cout << "Indexing edge list..." << std::endl;

    std::ofstream output(out, std::ios::binary);
    output.write(reinterpret_cast<const char*>(&EDGE_LIST_MAGIC), sizeof(EDGE_LIST_MAGIC));
    output.write(reinterpret_cast<const char*>(&_header), sizeof(_header));

    std::ifstream input(_tmp_filename, std::ios::binary);

    if(_has_weight)
    {
        stream_source<weighted_edge> source(input);

        if(_is_compressed)
        {
            zlib_stream_sink<weighted_edge> sink(output,
                 Z_DEFAULT_COMPRESSION, output.tellp());
            cat_source_to_sink<weighted_edge>(source, sink);
            sink.flush();
        }
        else
        {
            stream_sink<weighted_edge> sink(output, output.tellp());
            cat_source_to_sink<weighted_edge>(source, sink);
            sink.flush();
        }
    }
    else
    {
        stream_source<edge> source(input);

        if(_is_compressed)
        {
            zlib_stream_sink<edge> sink(output,
                Z_DEFAULT_COMPRESSION, output.tellp());
            cat_source_to_sink<edge>(source, sink);
            sink.flush();
        }
        else
        {
            stream_sink<edge> sink(output, output.tellp());
            cat_source_to_sink<edge>(source, sink);
            sink.flush();
        }
    }

    std::filesystem::remove(_tmp_filename);

}

template <typename Edge, typename Source, typename Sink>
void cat_source_to_sink(Source& source, Sink& sink)
{
    Edge e;
    while(source.read(e))
        sink.write(e);
}

