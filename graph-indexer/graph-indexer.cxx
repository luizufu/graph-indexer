#include <graph-indexer/cxxopts/cxxopts.hpp>
#include <graph-indexer/indexer/edge-list-indexer.hxx>
#include <graph-indexer/indexer/adj-list-indexer.hxx>
#include <graph-indexer/indexer/edge-grid-indexer.hxx>
#include <iostream>


cxxopts::ParseResult parse(int argc, char* argv[]);

int main (int argc, char* argv[])
{
    using namespace std;

    auto result = parse(argc, argv);

    auto positional = result["positional"].as<vector<string>>();
    if(result.count("positional") < 2)
    {
        cerr << "Missing positional arguments [input output]" << endl;
        return 1;
    }

    string format = result["format"].as<string>();
    if(!(format == "edge-list" ||
         format == "adj-list"  ||
         format == "edge-grid"))
    {
        cerr << format
            << " is not a valid option for parameter format"
            << "should be edge-list, adj-list or edge-grid" << endl;
        return 1;
    }

    bool index_incoming = result["index-incoming"].as<bool>();
    bool index_outgoing = result["index-outgoing"].as<bool>();
    if(format == "adj-list" && !(index_outgoing || index_incoming))
    {
        cerr << "Missing --index-outgoing or --index-incoming parameter" << endl;
        return 1;
    }

    string type = result["type"].as<string>();
    if(!(type == "direct"   ||
         type == "undirect"))
    {
        cerr << type
            << " is not a valid option for parameter type"
            << "should be direct, or undirect" << endl;
        return 1;
    }

    bool has_weight = result["weighted"].as<bool>();
    bool is_dynamic = result["dynamic"].as<bool>();
    bool is_compressed = result["compressed"].as<bool>();
    bool do_sort = result["sort"].as<bool>();
    bool remove_duplicate = result["remove-duplicates"].as<bool>();
    bool remove_self_loops = result["remove-self-loops"].as<bool>();

    uint32_t base = result["radix-base"].as<uint32_t>();

    srand(time(NULL));

    if(format == "edge-list")
    {
        edge_list_indexer index(positional[0],
            type, has_weight, is_compressed, remove_self_loops);

        if(do_sort)
            index.sort(base);

        if(remove_duplicate)
            index.remove_duplicate();

        index.make_index(positional[1]);
    }
    else if(format == "adj-list")
    {
        adj_list_indexer index(positional[0],
                                type, has_weight, is_dynamic, is_compressed,
                                remove_self_loops,
                                index_outgoing, index_incoming,
                                base);
        if(do_sort)
            index.sort(base);

        if(remove_duplicate)
            index.remove_duplicate();

        index.make_index(positional[1]);
    }
    else
    {
        uint32_t grid_size = result["grid-size"].as<uint32_t>();

        edge_grid_indexer index(positional[0],
                                type, has_weight, is_dynamic, is_compressed,
                                remove_self_loops,
                                grid_size, base,
                                result["max-vertex"].as<uint32_t>());

        if(do_sort)
            index.sort(base);

        if(remove_duplicate)
            index.remove_duplicate();

        index.make_index(positional[1]);
    }
}

cxxopts::ParseResult parse(int argc, char* argv[])
{
    using namespace std;

    try {
        cxxopts::Options options("graph-indexer",
         "graph-indexer indexes a text file containing an edge list");

        options.positional_help("input_file output_file").show_positional_help();

        options.add_options("Basic")
            ("f,format", "Format of the graph. Options are:  edge-list, adj-list, and edge-grid",
             cxxopts::value<string>()->default_value("adj-list"))
            ("index-outgoing", "Index outgoing edges if --format=adj-list",
             cxxopts::value<bool>()->default_value("false"))
            ("index-incoming", "Index incoming edges if --format=adj-list",
             cxxopts::value<bool>()->default_value("false"))
            ("t,type", "Type of the graph. Options are: direct, and undirect",
             cxxopts::value<string>()->default_value("direct"))
            ("w,weighted", "Whether the graph has weight on edges",
             cxxopts::value<bool>()->default_value("false"))
            ("d,dynamic", "Whether the graph can grow/shrink afterwards (use b+-tree)",
             cxxopts::value<bool>()->default_value("false"))
            ("c,compressed", "Whether the output file is compressed (zlib if --format=edge-list/edge-grid, and delta-gap if --format=adj-list)",
             cxxopts::value<bool>()->default_value("false"));

        options.add_options("Preprocessing")
            ("sort", "Sort data in ascend order using the radix sort algorithm ",
             cxxopts::value<bool>()->default_value("false"))
            ("remove-duplicates", "Remove duplicate edges",
             cxxopts::value<bool>()->default_value("false"))
            ("remove-self-loops", "Remove edges from a vertex to itself",
             cxxopts::value<bool>()->default_value("false"));

        options.add_options("Advanced")
            ("radix-base", "Set the radix base for the radix sort algorithm when --sort is set",
             cxxopts::value<uint32_t>()->default_value("8"))
            ("grid-size", "Set grid size when --format=edge-grid (total cells = grid-size x grid-size)",
             cxxopts::value<uint32_t>()->default_value("2"))
            ("max-vertex", "Set maximum vertex, used when --format=edge-grid and --dynamic are set",
             cxxopts::value<uint32_t>()->default_value("9"));

        options.add_options("Other")
            ("positional", "Positional arguments are: input_file output_dir. Parameter --positional can omitted if input_file output_dir are the last two arguments ",
             cxxopts::value<std::vector<std::string>>())
            ("h,help", "Print help");

        options.parse_positional("positional");
        auto result = options.parse(argc, argv);

        if(result.count("help"))
        {
            cout << options.help({"Basic", "Preprocessing", "Advanced", "Other"}) << endl;
            exit(0);
        }

        return result;

    } catch(const cxxopts::OptionException& e) {
        cerr << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
}
