#pragma once

#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/sink.hxx>

#include <sstream>
#include <filesystem>
#include <time.h>

using namespace stg::detail;


inline std::string gen_tmp(const int len)
{
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    std::string s(len, ' ');

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return s;
}

inline void cat_stream(std::istream& is, std::ostream& os)
{
    const std::streamsize buff_size = 1 << 16;
    char * buff = new char [buff_size];
    while(true)
    {
        is.read(buff, buff_size);
        std::streamsize cnt = is.gcount();
        if (cnt == 0) break;
        os.write(buff, cnt);
    }
    delete [] buff;
}

template <typename Edge>
void cat_edge_stream_txt2bin(std::istream& in, std::ostream& out,
                             bool is_direct, bool remove_self_loops,
                             bool swap_vertices = false)
{
    stream_sink<Edge> sink(out);

    Edge e;
    while(in >> e)
    {
        if(remove_self_loops && e.u == e.v)
            continue;

        if(!is_direct && e.v < e.u)
            std::swap(e.v, e.u);

        if(swap_vertices)
            std::swap(e.u, e.v);

        sink.write(e);
    }
}

template <typename Edge>
uint32_t cat_edge_stream_nodup(std::istream& in, std::ostream& out)
{
    stream_source<Edge> source(in);
    stream_sink<Edge> sink(out);

    Edge last_edge;
    Edge current_edge;

    uint32_t n_edges = 0;

    if(source.read(last_edge))
    {
        sink.write(last_edge);
        ++n_edges;
    }

    while(source.read(current_edge))
    {
        if(current_edge.e != last_edge.e)
        {
            sink.write(current_edge);
            ++n_edges;
        }

        last_edge = current_edge;
    }

    return n_edges;
}

template <typename Edge>
uint32_t find_max_vertex(std::istream& in)
{
    stream_source<Edge> source(in);

    Edge e;
    uint32_t max = 0;

    while(source.read(e))
    {
        max = std::max(max, e.u);
        max = std::max(max, e.v);
    }

    return max;
}

template <typename Edge>
std::pair<size_t, size_t> find_VE(std::istream& in)
{
    stream_source<Edge> source(in);

    Edge e;
    uint32_t max = 0;
    size_t n_edges = 0;


    while(source.read(e))
    {
        max = std::max(max, e.u);
        max = std::max(max, e.v);
        ++n_edges;
    }

    return { max + 1, n_edges };
}
