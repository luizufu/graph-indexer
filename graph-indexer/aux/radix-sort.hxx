#pragma once

#include <cstdint>
#include <cstddef>
#include <vector>

struct default_extractor
{
    template <typename T>
    T operator()(T number) const
    {
        return number;
    }
};

template <typename T, typename NumberExtractor>
void counting_sort(T* begin1, T* end1, T* begin2,
                   uint64_t exp, size_t base, NumberExtractor extractor)
{
    std::vector<size_t> count(base, 0u);

    for(auto it = begin1; it != end1; ++it)
        ++count[(extractor(*it) / exp) % base];

    for(size_t i = 1; i < count.size(); ++i)
        count[i] += count[i - 1];

    auto rbegin1 = end1 - 1;
    auto rend1 = begin1 - 1;
    for(auto it = rbegin1; it != rend1; --it)
    {
        size_t bucket = (extractor(*it) / exp) % base;
        --count[bucket];
        *(begin2 + count[bucket]) = *it;
    }
}

template <typename T, typename NumberExtractor>
void radix_sort(T* begin1, T* end1, T* begin2,
                size_t base, NumberExtractor extractor)
{
    uint64_t max = extractor(*begin1);
    for(auto it = begin1 + 1; it != end1; ++it)
        max = std::max(max, extractor(*it));

    bool forward = true;
    size_t size = end1 - begin1;
    for(uint64_t exp = 1; max / exp > 0; exp *= base)
    {
        if(forward)
            counting_sort(begin1, end1, begin2, exp, base, extractor);
        else
            counting_sort(begin2, begin2 + size, begin1, exp, base, extractor);

        forward = !forward;
    }

    // last was backwards
    if(forward)
        std::copy(begin1, end1, begin2);
}
