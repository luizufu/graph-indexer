# graph-indexer - C++ executable

This program receives a text file containing a list of edges and convert it to a binary format supported by [graph-reader](https://bitbucket.org/luizufu/graph-reader).
Currently there are 3 disk-based data structures (adjacency list, edge list, edge grid) with support for weight on edges and compression using zlib.

Usage:

![program usage](https://bitbucket.org/luizufu/graph-indexer/raw/67f91e31a1460c22ab146ad0db4ebc7a1d7a7d46/usage.png "Program usage")




